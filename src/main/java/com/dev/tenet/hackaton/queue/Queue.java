package com.dev.tenet.hackaton.queue;

public interface Queue<T> {
    T take();
}
