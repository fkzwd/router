package com.dev.tenet.hackaton;

import java.util.List;

public abstract class AbstractStep {
    private final static int STEP_ID = 1;

    private UserStateService userStateService;

    static class FieldDescription {
        long id;
        String type;
        String value;
    }

    static interface UserStateService {
        //user id
        //operation_id

        List<FieldDescription> getFields();
        void send(List<FieldDescription> fields);
    }



    int execute(List<FieldDescription> current) {
        List<FieldDescription> filledBefore = userStateService.getFields();
        int state = execute(current, filledBefore);
        sendToState(current);
        return state;
    }

    abstract int execute(List<FieldDescription> current, List<FieldDescription> before);
    public final void sendToState(List<FieldDescription> afterExecuteRequired) {
        if (afterExecuteRequired.size() == 0) {
            return;
        }
        else {
            userStateService.send(afterExecuteRequired);
        }
    }
}
