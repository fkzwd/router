FROM openjdk:11-jdk
ADD target/router-1.0.jar router.jar
ENV TZ=Europe/Moscow
ENTRYPOINT exec java $JAVA_OPTS -jar /router.jar